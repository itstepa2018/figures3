import figures.*;

public class Main {
    public static void main(String[] args) {
//22222222
        Circle circle = new Circle(0, 0, 5);
        Triangle triangle = new Triangle(
                new Point(-3, 0),
                new Point(0, 4),
                new Point(3, 0));
        Shape[] shapes = new Shape[6];
        shapes[0] = circle;
        shapes[1] = triangle;
        shapes[2] = new Circle(2, 6, 4);
        shapes[3] = new Triangle(
                new Point(3, 0),
                new Point(0, 4),
                new Point(6, 0));
        shapes[4] = new Square(new Point(0, 0),
                new Point(0, 2),
                new Point(2, 2),
                new Point(2, 0));
        shapes[5] = new Point(0, 0);

        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].toString());
        }
        for (int i = 0; i < shapes.length; i++) {
            if (shapes[i] instanceof SolidFigure) {
                SolidFigure solidFigure = (SolidFigure) shapes[i];
                System.out.println(solidFigure.getArea());
            } else {
                System.out.println("Object #" + i + " has no area");
            }
        }
        System.out.println(shapes[2].equals(new Circle(2,6,4)));
        System.out.println(triangle.calculateArea());
    }
}
