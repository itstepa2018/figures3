package figures;

public abstract class Tetragon extends SolidFigure {
    private double x1;
    private double x2;
    private double x3;
    private double x4;
    private double y1;
    private double y2;
    private double y3;
    private double y4;

    public Tetragon(Point point1, Point point2, Point point3, Point point4) {
        super((point1.getCenterX() + point2.getCenterX() + point3.getCenterX() + point4.getCenterX()) / 4,
                ((point1.getCenterY() + point2.getCenterY() + point3.getCenterY() + point4.getCenterY())) / 4);
        this.x1 = point1.getCenterX();
        this.x2 = point2.getCenterX();
        this.x3 = point3.getCenterX();
        this.x4 = point4.getCenterX();
        this.y1 = point1.getCenterY();
        this.y2 = point2.getCenterY();
        this.y3 = point3.getCenterY();
        this.y4 = point4.getCenterY();
    }

    public double getX1() {
        return x1;
    }

    public double getX2() {
        return x2;
    }

    public double getX3() {
        return x3;
    }

    public double getX4() {
        return x4;
    }

    public double getY1() {
        return y1;
    }

    public double getY2() {
        return y2;
    }

    public double getY3() {
        return y3;
    }

    public double getY4() {
        return y4;
    }

    @Override
    public String toString() {
        return "x1: " + x1 +
                ", x2: " + x2 +
                ", x3: " + x3 +
                ", x4: " + x4 +
                ", y1: " + y1 +
                ", y2: " + y2 +
                ", y3: " + y3 +
                ", y4: " + y4;
    }
}
