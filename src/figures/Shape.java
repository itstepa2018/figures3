package figures;

public abstract class Shape {
    private double centerX;
    private double centerY;

    public Shape(double centerX, double centerY) {
        this.centerX = centerX;
        this.centerY = centerY;
    }

    public double getCenterX() {
        return centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    @Override
    public String toString() {
        return "centerX = " + centerX + ", centerY = " + centerY;
    }
}
