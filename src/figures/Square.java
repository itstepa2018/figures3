package figures;

public class Square extends Tetragon {
    public Square(Point point1, Point point2, Point point3, Point point4) {
        super(point1, point2, point3, point4);
        setArea(calculateArea());
        setPerimeter(calculatePerimeter());
    }

    @Override
    public double calculateArea() {
        return (getX2() - getX1()) * (getY2() - getY1());
    }

    @Override
    public double calculatePerimeter() {
        return (getX2() - getX1())*4;
    }

    @Override
    public String toString() {
        return "Square{ " + super.toString() +
                " }";
    }
}
