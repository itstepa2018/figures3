package figures;

public class Triangle extends SolidFigure {
    private Point point1;
    private Point point2;
    private Point point3;


    public Triangle(Point point1, Point point2, Point point3) {
        super(0, 0);
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
        //TODO calculate centre
    }

    @Override
    public double calculateArea() {
        //TODO

        double p = (point1.calculateDistance(point2)
                + point2.calculateDistance(point3) +
                point3.calculateDistance(point1)) / 2;

        return Math.sqrt(p * (p - point1.calculateDistance(point2)) *
                (p - point2.calculateDistance(point3)) *
                (p - point3.calculateDistance(point1)));
    }

    @Override
    public String toString() {
        return "Triangle{ " +
                "point1: " + point1 +
                ", point2: " + point2 +
                ", point3: " + point3 +
                '}';
    }

    @Override
    public double calculatePerimeter() {
        return point1.calculateDistance(point2)
                + point2.calculateDistance(point3)
                + point3.calculateDistance(point1);
    }
}
