package figures;

public class Point extends Shape {
    public Point(double centerX, double centerY) {
        super(centerX, centerY);
    }

    public double calculateDistance(Point point){
        double xDifference = point.getCenterX() - this.getCenterX();
        xDifference = Math.abs(xDifference);
        double yDifference = point.getCenterY() - this.getCenterY();
        yDifference = Math.abs(yDifference);
        double lineSize = Math.sqrt((xDifference * xDifference)
                + (yDifference * yDifference));
        return lineSize;
    }
}
