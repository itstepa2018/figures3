package figures;

public abstract class SolidFigure extends Shape implements Spatial {
    private double area;
    private double perimeter;

    public SolidFigure(double centerX, double centerY) {
        super(centerX, centerY);
    }

    public double getArea() {
        return area;
    }

    public double getPerimeter() {
        return perimeter;
    }

    protected void setArea(double area) {
        this.area = area;
    }

    protected void setPerimeter(double perimeter){
        this.perimeter = perimeter;
    }


}
